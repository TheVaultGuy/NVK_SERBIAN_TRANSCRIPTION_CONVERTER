files="$@"
filesSize=0

r=$(printf "\r")
n=$(printf "\n")

clear
echo -e "\n\tWelcome to the NVK Transcription Formatter.\n\tPress Ctrl+C at any time to cancel and exit.\n\n"

while true; do
    if [[ -z $files ]]; then
        inFiles="something"
        read -p "Drag the raw .rtf file(s) into this window. Press Enter when done: " inFiles
        while read -r line; do
            if [[ -f $line ]]; then
                
                files[filesSize]="$line"
                ((filesSize++))
            else
                echo -e "\n\n--- ERROR:\n \"$line\" is not a valid file"
            fi
        done < <(sed -e $'s/ \//\\\n\//g'<<<"$inFiles")
    fi
    
    for file in "${files[@]}"; do
        filePath=${file%/*}
        fileBase=${file##*/}
        fileName=${fileBase%.*}
        
        newFile="$filePath/$fileName.txt"
        
        if [[ -f $newFile ]]; then
            echo ""
            echo ""
            echo "Error converting $fileBase. $newFile already exists."
        else
            echo -e "\n\n\tConverting $fileBase"
            headerOver=false
            lastBlank=false
            lastTimecode=""
            while read line; do
                if [[ "$headerOver" = true ]]; then
                    filtered="$(echo -n "$line" | sed -E 's/		/ /g; s/[\\
.*]//g ; s/ [ ]+/ /g; s/[ ]+$//g; s/([0-9])[!;]([0-9])/\1:\2/g')"
                    if [[ "$filtered" =~ ^[^a-zA-Z0-9]*$ ]]; then
                        if [[ "$lastBlank" = false ]]; then
                            printf "$r"
                            printf "$r"
                            lastBlank=true
                        fi
                    else
                        if [[ "$filtered" =~ .*[0-9]+:[0-9]+.* ]]; then
                            oldBase=$(printf "$lastTimecode" | sed -E "s/^(.*):[0-9]+$/\1/g")
                            oldTail=$(printf "$lastTimecode" | sed -E "s/^.*:([0-9]+)$/\1/g")
                            newTimecode=$(printf "$filtered" | sed -E "s/^([0-9:]+) [0-9:]+.*$/\1/g")
                            newBase=$(printf "$newTimecode" | sed -E "s/^(.*):[0-9]+$/\1/g")
                            newTail=$(printf "$newTimecode" | sed -E "s/^.*:([0-9]+)$/\1/g")
                            threshold=$((10#$oldTail+2))
                            if [[ "$oldBase" == "$newBase" && $newTail < $threshold ]]; then
                                fixedLine=$(printf "$filtered" | sed -E "s/^[0-9:]+(.*)$/$newBase:$oldTail\1/g")
                                printf "$fixedLine$r"
                                lastBlank=false
                            else
                                printf "$filtered$r"
                                lastBlank=false
                            fi
                            lastTimecode=$(printf "$filtered" | sed -E "s/^.*[0-9:]+ ([0-9:]+).*$/\1/g")
                        else
                            printf "$filtered$r"
                            lastBlank=false
                        fi
                    fi
                else                
                    if [[ "$line" =~ .*[0-9]+:[0-9]+.* ]]; then
                        headerOver=true
                        printf "@ $fileName$r"
                        printf "$r"
                        printf "<begin subtitles>$r"
                        printf "$r"
                        firstLine=$(printf "$line" | sed -E "s/^.* ([0-9: 	]+).*$/\1/; s/		/ /g; s/[$r$n].*$//g; s/ [ ]+/ /g; s/[ ]+$//g; s/;/:/g")
                        printf "$firstLine"
                        printf "$r"
                        lastTimecode=$(printf "$firstLine" | sed -E "s/^.*[0-9:]+ ([0-9:]+).*$/\1/g")
                    fi
                fi
            done <"$file" > "$newFile"
            
            if [[ "$lastBlank" = false ]]; then
                printf "$r" >> "$newFile"
                lastBlank=true
            fi
            printf "<end subtitles>$r" >> "$newFile"
        fi
        
        
    done
    
    files=
    filesSize=0
    
    echo ""
    echo ""
    echo -e "\tFinished!"
    echo ""
    echo ""
done